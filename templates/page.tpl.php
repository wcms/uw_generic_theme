<?php

/**
 * @file
 */
global $base_path; ?>
<div id="site" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div id="skip">
    <a href="#main" class="element-invisible element-focusable"><?php print t('Skip to main'); ?></a>
    <a href="#footer" class="element-invisible element-focusable"><?php print t('Skip to footer'); ?></a>
  </div>
  <div id="header">
    <div id="uw-header" class="clearfix<?php if (variable_get('uw_generic_theme_generic_uwwordmark') != 'yes' && variable_get('uw_generic_theme_generic_uwwordmark') != 'both') { ?> nologo<?php
   } ?>">
      <?php if (variable_get('uw_generic_theme_generic_uwwordmark') == 'yes' || variable_get('uw_generic_theme_generic_uwwordmark') == 'both') { ?>
      <a id="uw-logo" href="//uwaterloo.ca/">University of Waterloo</a>
      <!--
      <a id="uw-wordmark" href="//uwaterloo.ca/">
      <img src="<?php print $base_path . drupal_get_path('theme', 'uw_core_theme'); ?>/images/logo-160x34.png" alt="University of Waterloo"/>
      </a>
      -->
      <?php } ?>
      <!-- search -->
<?php uw_core_theme_output_search_box(); ?>
    </div>
    <div id="site-header"><a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/></a></div>
    <?php print render($page['banner_alt']); ?>
    <div id="site-navigation"><?php print render($page['sidebar_first']); ?></div>
  </div><!--/header-->
  <div id="main" class="clearfix" role="main">
    <?php print render($page['banner']); ?>
    <?php print $messages; ?>
    <?php print render($page['help']); ?>
    <?php print $breadcrumb; ?>
    <?php list($title, $content) = uw_fdsu_theme_separate_h1_and_content($page, $title); ?>
    <h1><?php print $title ? $title : $site_name; ?></h1>
    <?php if ($tabs): ?><div class="node-tabs"><?php print render($tabs); ?></div><?php
    endif; ?>
    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php
    endif; ?>
    <div id="content">
      <?php print $content; ?>
    </div><!--/main-content-->
    <div id="site-sidebar">
      <?php print render($page['promo']); ?>
      <?php print render($page['sidebar_second']); ?>
    </div>
  </div><!--/main-->
  <div id="footer" role="contentinfo">
    <?php if (variable_get('uw_generic_theme_generic_uwwordmark') == 'both') { ?>
      <div id="watermark"></div>
    <?php } ?>
    <?php if (!empty($page['site_footer'])): ?><div id="site-footer" class="clearfix"><?php print render($page['site_footer']); ?></div><?php
    endif; ?>
    <div id="uw-footer" class="clearfix"></div>
    <?php print render($page['login_link']); ?>
  </div><!--/footer-->
</div><!--/site-->
