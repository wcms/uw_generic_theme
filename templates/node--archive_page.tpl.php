<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php

   $query_submit = "SELECT u.name FROM {users} u, {node} n WHERE n.uid=u.uid AND n.nid = :nodeid;";
   $query = "SELECT u.name FROM {node_revision} nr, {users} u WHERE nr.uid = u.uid AND nr.nid = :nodeid ORDER BY timestamp DESC LIMIT 1";

   $args = array(
     ':nodeid' => $node->nid,
   );
   $submit_author_uid = db_query($query_submit, $args)->fetchField();
   $update_author_uid = db_query($query, $args)->fetchField();

   $display_submit = $submit_author_uid;
   $display_modify = $update_author_uid;

   if(module_exists("uw_ldap")){
     $data_submit = uw_ldap_pub_lookup($submit_author_uid);
     $display_submit = $data_submit["displayname"][0];
     $data_modify = uw_ldap_pub_lookup($update_author_uid);
     $display_modify = $data_modify["displayname"][0];
   }

  ?>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
     <?php if ($display_submitted): ?>
    <div class="submitted" >
      <?php
       if (in_array('administrator', $current_user_roles) || in_array('site manager', $current_user_roles)){
         print t('Original description created by <a href="@url">@display_submit</a> on @created.', array('@url' => url('users/' . $submit_author_uid), '@display_submit' => $display_submit, '@created' => format_date($node->created, 'custom', 'd M Y')));
         print '<br>';
         print t('Last modified by <a href="@url">@display_modify</a> on @changed.', array('@url' => url('users/' . $update_author_uid), '@display_modify' => $display_modify, '@changed' => format_date($node->changed, 'custom', 'd M Y')));
       }
       elseif (in_array('anonymous user', $current_user_roles) || in_array('content editor', $current_user_roles) || in_array('content author', $current_user_roles)){
         print t('Original description created by @display_submit on @created.', array('@display_submit' => $display_submit, '@created' => format_date($node->created, 'custom', 'd M Y')));
         print '<br>';
         print t('Last modified by @display_modify on @changed.', array('@display_modify' => $display_modify, '@changed' => format_date($node->changed, 'custom', "d M Y")));
       }
      ?>
    </div>
     <?php endif; ?>
  </div>
  <?php print render($content['links']); ?>
  <?php print render($content['comments']); ?>
</div>
