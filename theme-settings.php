<?php

/**
 * @file
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function uw_generic_theme_form_system_theme_settings_alter(&$form, $form_state) {
  $form['generic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Generic theme settings'),
  );
  $form['generic']['uwwordmark'] = array(
    '#type' => 'select',
    '#title' => t('Show University of Waterloo wordmark and watermark?'),
    '#default_value' => variable_get('uw_generic_theme_generic_uwwordmark', 'no'),
    '#options' => array(
      'no' => t('No'),
      'yes' => t('Wordmark only'),
      'both' => t('Both wordmark and watermark'),
    ),
  );
  $form['#submit'][] = 'uw_generic_theme_set_generic_uwwordmark_variable';
}

/**
 *
 */
function uw_generic_theme_set_generic_uwwordmark_variable($form, &$form_state) {
  if (isset($form_state['values']['uwwordmark'])) {
    variable_set('uw_generic_theme_generic_uwwordmark', $form_state['values']['uwwordmark']);
  }
}
