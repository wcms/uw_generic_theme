<?php

/**
 * @file
 */

/**
 *
 */
function uw_generic_theme_preprocess_node(&$variables) {
  global $user;
  $variables['current_user_roles'] = $user->roles;
}
